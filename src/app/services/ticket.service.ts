import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(protected http: HttpClient) {
  }

  /**
   * [URL]
   * @description request endpoint
   * @protected
   * @memberof TicketService
   */
  protected URL = `${env.API}ticket`

  /**
   * [getTicket]
   * @description Service that get the information for all tickets
   * @memberof TicketService
   */
  getTickets(params?: HttpParams): Observable<any> {
    return this.http.get<any>(this.URL, { params });
  }

  /**
   * [getTicket]
   * @description Service that get the information for an specific ticket
   * @memberof TicketService
   */
  getTicket(params?: HttpParams): Observable<any> {
    return this.http.get<any>(`${this.URL}/`, { params });
  }

  /**
   * [createTicket]
   * @description Service that create a new ticket
   * @memberof TicketService
   */
  createTicket(body: any): Observable<any> {
    return this.http.post(this.URL, body);
  }

  /**
   * [deleteTicket]
   * @description Service that delete a ticket
   * @memberof TicketService
   */
  deleteTicket(params?: HttpParams): Observable<any> {
    return this.http.delete(`${this.URL}/`, { params });
  }

  /**
   * [deleteTickets]
   * @description Service that delete tickets
   * @memberof TicketService
   */
  deleteTickets(body: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body
    };
    return this.http.delete(this.URL, httpOptions);
  }

}
