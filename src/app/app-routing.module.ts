import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PqrModule } from './modules/pqr/pqr.module';

const routes: Routes = [
  {
    loadChildren: (): Promise<PqrModule> =>
      import('src/app/modules/pqr/pqr.module')
        .then(async (m: any): Promise<PqrModule> => m.PqrModule),
    path: 'inicio'
  },
  { path: '**', redirectTo: '/inicio' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
