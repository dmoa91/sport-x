import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PqrRoutingModule } from './pqr-routing.module';
import { HomeComponent } from './components/home/home.component';
import { PqrListComponent } from './components/pqr-list/pqr-list.component';
import { CreatePqrComponent } from './components/create-pqr/create-pqr.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewTicketInformationComponent } from './components/view-ticket-information/view-ticket-information.component';


@NgModule({
  declarations: [
    HomeComponent,
    PqrListComponent,
    CreatePqrComponent,
    ViewTicketInformationComponent
  ],
  imports: [
    CommonModule,
    PqrRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PqrModule { }
