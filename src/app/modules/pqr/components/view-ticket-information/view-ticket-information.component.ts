import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-view-ticket-information',
  templateUrl: './view-ticket-information.component.html',
  styleUrls: ['./view-ticket-information.component.scss']
})
export class ViewTicketInformationComponent implements OnInit {

  /**
   * [ticketId]
   * @description Ticket id number
   * @memberof PqrListComponent
   */
  @Input() ticketId: any;

  /**
   * [ticket]
   * @description Store the ticket of the consult
   * @memberof PqrListComponent
   */
  public ticket: any;

  /**
   * [params]
   * @description Stores the params of the query
   * @private
   * @type {HttpParams}
   * @memberof ViewTicketInformationComponent
   */
  private params: HttpParams;

  /**
   * [ticket_types]
   * @description Ticket types select
   * @type {string}
   * @memberof PqrListComponent
   */
  public ticket_types: any = {
    "0": "Petición",
    "1": "Queja",
    "2": "Reclamo"
  }

  /**
   * [state]
   * @description Ticket Status select
   * @type {string}
   * @memberof PqrListComponent
   */
  public state: any = {
    "true": "Abierto",
    "false": "Cerrado",
  }

  /**
   * [identificationTypes]
   * @description Identification types select
   * @type {string}
   * @memberof PqrListComponent
   */
  public identificationTypes: any = {
    "0": "Cédula de ciudadania",
    "1": "Cédula de extranjeria",
    "2": "Tarjeta de identidad"
  }

  constructor(
    public modal: NgbActiveModal,
    private _ticketService: TicketService
  ) {
    this.params = new HttpParams();
  }

  ngOnInit(): void {
    this.params = new HttpParams().set("id", this.ticketId.toString())
    this.getTicket(this.params);
  }

  /**
   * this method retrieves a ticket information by id
   */
  public getTicket(params?: HttpParams) {
    this._ticketService.getTicket(params).subscribe((data: any) => {
      this.ticket = data.ticket_data
    })
  }
  /**
   * this method close the modal
   */
  onBack() {
    this.modal.close();
  }

}
