import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTicketInformationComponent } from './view-ticket-information.component';

describe('ViewTicketInformationComponent', () => {
  let component: ViewTicketInformationComponent;
  let fixture: ComponentFixture<ViewTicketInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewTicketInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTicketInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
