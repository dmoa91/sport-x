import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TicketService } from 'src/app/services/ticket.service';
import { PqrListComponent } from '../pqr-list/pqr-list.component';

@Component({
  selector: 'app-create-pqr',
  templateUrl: './create-pqr.component.html',
  styleUrls: ['./create-pqr.component.scss']
})
export class CreatePqrComponent implements OnInit {

  form = new FormGroup({
    ni: new FormControl(),
    type_doc: new FormControl(),
    name: new FormControl(),
    last_name: new FormControl(),
    cellphone: new FormControl(),
    phone: new FormControl(),
    email: new FormControl(),
    title: new FormControl(),
    type_ticket: new FormControl(),
    description: new FormControl()
  })
  constructor(
    public modal: NgbActiveModal,
    private _ticketService: TicketService,
  ) { }

  ngOnInit(): void {
  }

  /**
   * this method create a new ticket and close the modal
   */
  onSubmit() {
    this._ticketService.createTicket(this.form.value).subscribe((data: any) => {
      this.modal.close();
      alert(`Ticket ${data.ticket_id} creado exitosamente`)
    }, (error => {
      alert('Ha ocurrido un error')
    }));
  }

  /**
   * this method close create pqr modal
   */
  onClose() {
    this.modal.close();
  }

}
