import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TicketService } from 'src/app/services/ticket.service';
import { CreatePqrComponent } from '../create-pqr/create-pqr.component';
import { ViewTicketInformationComponent } from '../view-ticket-information/view-ticket-information.component';

@Component({
  selector: 'app-pqr-list',
  templateUrl: './pqr-list.component.html',
  styleUrls: ['./pqr-list.component.scss']
})
export class PqrListComponent implements OnInit {

  /**
   * [columns]
   * @description Columns Table
   * @type {string}
   * @memberof PqrListComponent
   */
  public columns = [
    "ID",
    "Estado",
    "Tipo de ticket",
    "Cliente",
    "Acciones"
  ]

  /**
   * [state]
   * @description Ticket Status select
   * @type {string}
   * @memberof PqrListComponent
   */
  public state: any = {
    "true": "Abierto",
    "false": "Cerrado",
  }
  /**
   * [ticket_types]
   * @description Ticket types select
   * @type {string}
   * @memberof PqrListComponent
   */
  public ticket_types: any = {
    "0": "Petición",
    "1": "Queja",
    "2": "Reclamo"
  }

  /**
   * [tickets]
   * @description Store the ticket of the consult
   * @memberof PqrListComponent
   */
  public tickets: any = [];

  /**
   * [params]
   * @description Stores the params of the query
   * @private
   * @type {HttpParams}
   * @memberof ViewTicketInformationComponent
   */
  private params: HttpParams;

  constructor(
    private modalService: NgbModal,
    private _ticketService: TicketService
  ) {
    this.params = new HttpParams();
  }

  ngOnInit(): void {
    this.ticketList();
  }

  /**
   * this method open selector dialog
   */
  public openCreatePqrDialog() {
    const modalRef = this.modalService.open(CreatePqrComponent, {
      centered: true,
      size: "lg",
    });
    modalRef.result.then(() => {
      this.ticketList();
    });
  }

  /**
   * this method open selector dialog
   * @param {string} ticketId Ticket id to view
   */
  public openViewInformationPqrDialog(ticketId: number) {
    const modalRef = this.modalService.open(ViewTicketInformationComponent, {
      centered: true,
      size: "lg",
    });
    modalRef.componentInstance.ticketId = ticketId;
  }

  /**
   * this method retrieves all tickets information
   */
  public ticketList() {
    this._ticketService.getTickets().subscribe((data: any) => {
      this.tickets = data.tickets
    })
  }

  /**
   * this method delete an specific ticket information
   * @param {string} ticketId Ticket id to delete
   */
  public deleteTicket(ticketId: number) {
    this.params = new HttpParams().set("id", ticketId.toString())
    this._ticketService.deleteTicket(this.params).subscribe((data: any) => {
      this.ticketList();
      console.log(data.ticket_id)
      alert(`Ticket ${data.ticket_id} eliminado exitosamente`)
    })
  }

}
