import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PqrListComponent } from './components/pqr-list/pqr-list.component';

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "lista-pqrs", component: PqrListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PqrRoutingModule { }
